import Vue from 'vue'
import VueChatScroll from 'vue-chat-scroll'
import App from './App'
import router from './router'
import firebase from 'firebase'
import './components/firebaseInit'

Vue.config.productionTip = false
Vue.use(VueChatScroll)

let app
firebase.auth().onAuthStateChanged(user => {
  if (!app) {
    app = new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
  }
})

