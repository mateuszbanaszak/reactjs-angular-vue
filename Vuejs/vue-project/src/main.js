import { createApp, onUnmounted, ref, computed } from 'vue'
import App from './App.vue'
import router from './router'
import firebase from 'firebase/app'
import "firebase/firestore"
import 'firebase/auth'
import './main.css'


const firebaseConfig = {
    apiKey: "AIzaSyD2HLLJM-wva4ESZlel1bt9RMGDrtbQ6nI",
    authDomain: "vue-project-ucn.firebaseapp.com",
    projectId: "vue-project-ucn",
    storageBucket: "vue-project-ucn.appspot.com",
    messagingSenderId: "816252172082",
    appId: "1:816252172082:web:51fa44960790ee5679492b"
  };

var fire = firebase.initializeApp(firebaseConfig);

const auth = firebase.auth()

export function useAuth() {
  const email = ref(null)
  const unsubscribe = auth.onAuthStateChanged(_email => (email.value = _email))
  onUnmounted(unsubscribe)
  const isLogin = computed(() => email.value !== null)

  return { email, isLogin}
}

const firestore = firebase.firestore()
const messagesCollection = firestore.collection('messages')
const messagesQuery = messagesCollection.orderBy('createdAt', 'desc').limit(100)

export function useChat() {
  const messages = ref([])
  const unsubscribe = messagesQuery.onSnapshot(snapshot => {
    messages.value = snapshot.docs 
      .map(doc => ({id: doc.id, ...doc.data()}))
      .reverse()
  })
  onUnmounted(unsubscribe)
  const { email, isLogin } = useAuth()
  const sendMessage = text => {
    if (!isLogin.value) return
    const {uid, displayName } = email.value
    messagesCollection.add({
      userName: displayName,
      userId: uid,
      text: text,
      createdAt: firebase.firestore.FieldValue.serverTimestamp()
    })
  }

  return { messages, sendMessage }
}



createApp(App).use(router).mount('#app')

export default fire;
