import { createRouter, createWebHistory } from 'vue-router'
import Profile from './Profile.vue'

const routes = [
    {
        path: '/profile',
        name: 'Profile',
        component: Profile
    },
    {
        path: '/',
        name: 'Home',
        component: Home
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
  })


export default router