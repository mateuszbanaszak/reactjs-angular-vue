import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Nested from './components/Nested.vue';

Vue.config.productionTip = false;

Vue.component('nested', Nested)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
