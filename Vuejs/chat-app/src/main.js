import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyD2HLLJM-wva4ESZlel1bt9RMGDrtbQ6nI",
  authDomain: "vue-project-ucn.firebaseapp.com",
  projectId: "vue-project-ucn",
  storageBucket: "vue-project-ucn.appspot.com",
  messagingSenderId: "816252172082",
  appId: "1:816252172082:web:51fa44960790ee5679492b"
};

firebase.initializeApp(firebaseConfig);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
